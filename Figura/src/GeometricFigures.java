import java.io.*;
import java.lang.Math;

class Circle implements Figure{
	double raza;
	
	Circle(){
		this.raza=0;
	}
	Circle(double raza){
		this.raza=raza;
	}
	
	public double area(){
		return Math.PI*raza*raza;
	}
	public double perimeter(){
		return Math.PI*2*raza;
	}
}

class Rectangle implements Figure{
	double latura1,latura2;
	
	Rectangle(){
		this.latura1=0;
		this.latura2=0;
	}
	Rectangle(double latura1,double latura2){
		this.latura1=latura1;
		this.latura2=latura2;
	}
	public double area(){
		return latura1*latura2;
	}
	public double perimeter(){
		return (latura1+latura2)*2;
	}
}

class Triangle implements Figure{
	double lat1,lat2,lat3,p;
	
	Triangle(){
		this.lat1=0;
		this.lat2=0;
		this.lat3=0;
	}
	Triangle(double lat1, double lat2, double lat3){
		this.lat1=lat1;
		this.lat2=lat2;
		this.lat3=lat3;
	}
	
	public double area(){
		p=(double)(lat1+lat2+lat3)/2;
		return Math.sqrt(p*(p-lat1)*(p-lat2)*(p-lat3));
	}
	public double perimeter(){
		return lat1+lat2+lat3;
	}
}


public class GeometricFigures{
	public static void main(String[] args) throws IOException{
		
		BufferedReader buff=new BufferedReader(new InputStreamReader(System.in));
		String sir=new String();
		System.out.println("Alegeti o figura geomtrica: Cerc Triunghi Dreptunghi");
		sir=buff.readLine();
		double num1,num2,num3;
		
		switch(sir){
		
		case "Cerc":
			
			System.out.println("Introduceti raza: ");
			sir=buff.readLine();
			num1=Double.parseDouble(sir);
			Figure cerc=new Circle(num1);
			System.out.println("Aria este: "+cerc.area());
			System.out.println("Circumferinta este: "+cerc.perimeter());
			
			break;
		
		case "Triunghi":
		
			System.out.println("Introduceti prima latura: ");
			sir=buff.readLine();
			num1=Double.parseDouble(sir);
			System.out.println("Introduceti a doua latura: ");	
			sir=buff.readLine();
			num2=Double.parseDouble(sir);
			System.out.println("Introduceti a treia latura: ");	
			sir=buff.readLine();
			num3=Double.parseDouble(sir);
			
			Figure triunghi=new Triangle(num1,num2,num3);
			System.out.println("Aria este: "+triunghi.area());
			System.out.println("Perimetrul este: "+triunghi.perimeter());
		
			break;
			
		case "Dreptunghi":
			
			System.out.println("Introduceti prima latura: ");
			sir=buff.readLine();
			num1=Double.parseDouble(sir);
			System.out.println("Introduceti a doua latura: ");	
			sir=buff.readLine();
			num2=Double.parseDouble(sir);
		
			Figure dreptunghi=new Rectangle(num1,num2);
			System.out.println("Aria este: "+dreptunghi.area());
			System.out.println("Perimetrul este: "+dreptunghi.perimeter());
			
			break;
			
		default: System.out.println("Figura invalida");
			break;
		}
		
	}
}